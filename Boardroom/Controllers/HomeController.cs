﻿using Boardroom.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Boardroom.Controllers {

    [Authorize]
    public class HomeController : Controller {

        private static HttpClient _client = new HttpClient();
        private const string _WEBINARS = "https://api.hubapi.com/content/api/v2/pages?slug__icontains=webinar&hapikey=6fa517d3-e654-49f3-92c9-d5dd1c5e9e5d";        
        

        public async Task<ActionResult> Index() {

            var model = new Dashboard();
            var response = await _client.GetAsync(_WEBINARS);
            
            if (response.IsSuccessStatusCode) {               
                model.Webinars = JObject.Parse(await response.Content.ReadAsStringAsync());
            }

            model.Token = TokenHelper.Encode(User.Identity.Name);

            return View(model);
        }
    }
}