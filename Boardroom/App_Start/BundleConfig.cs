﻿using System.Web.Optimization;

namespace Boardroom {
    public class BundleConfig {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                      "~/scripts/main.js"));

            bundles.Add(new StyleBundle("~/content/css").Include(
                      "~/content/main.css"));
        }
    }
}
