﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boardroom.Models {
    public class Dashboard {

        public JObject Webinars { get; set; }

        public string Token { get; set; }

    }
}