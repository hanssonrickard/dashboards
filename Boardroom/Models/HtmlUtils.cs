﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace Boardroom.Models {

    /// <summary>
    /// Provides extension and helper methods for generating html markup.
    /// </summary>
    public static class HtmlUtils {

        public static MvcHtmlString Name(this IIdentity identity) {

            if (identity is ClaimsIdentity ci) {
                return new MvcHtmlString(ci.FindFirst("name")?.Value);
            }
            return null;
        }
    }
}