﻿using JWT.Algorithms;
using JWT.Builder;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Http.Results;

namespace Boardroom.Models {
    public static class TokenHelper {

        public static string Encode(string sub) {
            return new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .AddClaim("exp", DateTimeOffset.UtcNow.AddSeconds(20).ToUnixTimeSeconds())
                .AddClaim("iss", "dashboard-app")
                .AddClaim("sub", sub)
                .AddClaim("email", sub)
                .AddClaim("client_id", ConfigurationManager.AppSettings["weavy:clientid"])
                .WithSecret(ConfigurationManager.AppSettings["weavy:clientsecret"])
                .Encode();
        }
    }
}